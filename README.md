# README #

run a docker container with OpenTripPlanner

```
#!bash

docker run -d --name="otp_berlin" -p 8080:8080 walihann/otp:v1
```

find a running instance on aws at: http://otp-imp.elasticbeanstalk.com/index.html

The container is pushed to https://registry.hub.docker.com/u/walihann/otp/
