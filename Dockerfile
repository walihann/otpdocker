#
# Open Trip Planner Dockerfile
#

# Pull base image.
FROM dockerfile/java:oracle-java7

MAINTAINER Hanno Walischewski "hanno@walischewski.net"

RUN apt-get update

# add the open trip planner jar to the container
ADD otp.jar /otp/

# add the graph from Open Street Map and GTFS data for Berlin to the container
ADD Graph.obj /var/otp/graphs/

# set working directory where the jar lives
WORKDIR /otp

# start otp
CMD java -Xmx4g -jar otp.jar --server

# publish the port of OTP
EXPOSE 8080
